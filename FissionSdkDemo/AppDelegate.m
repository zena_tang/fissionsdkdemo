//
//  AppDelegate.m
//  FissionSdkDemo
//
//  Created by zena.tang on 2020/12/17.
//  Copyright © 2020 fissionsdk. All rights reserved.
//

#import "AppDelegate.h"
#import <GoogleSignIn/GoogleSignIn.h>
#import "WXApi.h"
#import "WXLoginViewController.h"
#import <AlipaySDK/AlipaySDK.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
@import FissionSdk;
@import FissionAccount;

@interface AppDelegate ()<WXApiDelegate>

@end

//#define TEST_APP_HOST @"https://api-lovematchios.freeqingnovel.com"
#define TEST_APP_HOST @"https://api-test.freeqingnovel.com"
#define TEST_APP_KEY @"xjdN82dpqMf"

@implementation AppDelegate
@synthesize window = _window;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    // 登录
    [GIDSignIn sharedInstance].clientID = @"868844438271-1a67c7ekdj2b4mon07it0fbgdpe63v14.apps.googleusercontent.com";
    
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"Info"ofType:@"plist"];
            
    NSDictionary *dictionary = [[NSDictionary alloc] initWithContentsOfFile:plistPath];
            
    if (dictionary != nil && dictionary[@"socialAccount"]) {
        NSString *universalLink = dictionary[@"socialAccount"][@"universalLink"];
        NSDictionary *wxDic = dictionary[@"socialAccount"][@"weixin"];
        if (wxDic != nil) {
            NSString *wxAppID = wxDic[@"appID"];
            [WXApi registerApp:wxAppID universalLink:universalLink];
        }
             
    }
        
    
    //[FissionSdkManager setTestMode:YES];
        
    [FissionSdkManager initWithHost:TEST_APP_HOST key:TEST_APP_KEY deviceId:nil userId:nil];
    
    return YES;
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options{
    if ([url.absoluteString hasPrefix:@"wx"]){
        return [WXApi handleOpenURL:url delegate:self];
    } else if ([url.absoluteString hasPrefix:@"fb"]){
        [[FBSDKApplicationDelegate sharedInstance] application:app openURL:url options:options];
    } else if ([url.host isEqualToString:@"safepay"]) {
            // 支付跳转支付宝钱包进行支付，处理支付结果
            [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
                NSLog(@"result = %@",resultDic);
            }];
            
            // 授权跳转支付宝钱包进行支付，处理支付结果
            [[AlipaySDK defaultService] processAuth_V2Result:url standbyCallback:^(NSDictionary *resultDic) {
                NSLog(@"result = %@",resultDic);
                // 解析 auth code
                NSString *result = resultDic[@"result"];
                NSString *authCode = nil;
                if (result.length>0) {
                    NSArray *resultArr = [result componentsSeparatedByString:@"&"];
                    for (NSString *subResult in resultArr) {
                        if (subResult.length > 10 && [subResult hasPrefix:@"auth_code="]) {
                            authCode = [subResult substringFromIndex:10];
                            break;
                        }
                    }
                }
                NSLog(@"授权结果 authCode = %@", authCode?:@"");
            }];
    }
    
    return YES;
}

#pragma mark - WXApiDelegate
- (void)onResp:(BaseResp *)resp{
    if([resp isKindOfClass:[SendAuthResp class]]){
        SendAuthResp *resp2 = (SendAuthResp *)resp;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"wxLogin" object:resp2];
    }else{
        NSLog(@"授权失败");
    }
}




#pragma mark - UISceneSession lifecycle


- (UISceneConfiguration *)application:(UIApplication *)application configurationForConnectingSceneSession:(UISceneSession *)connectingSceneSession options:(UISceneConnectionOptions *)options {
    // Called when a new scene session is being created.
    // Use this method to select a configuration to create the new scene with.
    return [[UISceneConfiguration alloc] initWithName:@"Default Configuration" sessionRole:connectingSceneSession.role];
}


- (void)application:(UIApplication *)application didDiscardSceneSessions:(NSSet<UISceneSession *> *)sceneSessions {
    // Called when the user discards a scene session.
    // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
    // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
}


@end
