//
//  MissionTableViewCell.h
//  FissionSdkDemo
//
//  Created by zena.tang on 2021/1/21.
//  Copyright © 2021 fissionsdk. All rights reserved.
//

#import <UIKit/UIKit.h>
@import FissionSdk;

NS_ASSUME_NONNULL_BEGIN

@interface MissionTableViewCell : UITableViewCell

- (void)setItem:(FissionSdkMissionData *)item;

@end

NS_ASSUME_NONNULL_END
