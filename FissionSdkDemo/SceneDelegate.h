//
//  SceneDelegate.h
//  FissionSdkDemo
//
//  Created by zena.tang on 2020/12/17.
//  Copyright © 2020 fissionsdk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

