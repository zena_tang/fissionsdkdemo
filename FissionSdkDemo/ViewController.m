//
//  ViewController.m
//  FissionSdkDemo
//
//  Created by zena.tang on 2020/12/12.
//  Copyright © 2020 TaurusXAds. All rights reserved.
//

#import "ViewController.h"
#import "macro.h"
@import FissionSdk;
@import FissionAccount;
#import "WXLoginViewController.h"
#import "ShareViewController.h"
#import "ApprenticeViewController.h"
#import "InviteAwardViewController.h"
#import "UIView+Toast.h"
#import "MissionViewController.h"

#define WITHDRAW_TEST_MISSION_ID @""
#define WXWITHDRAW_TEST_MISSION_ID @""




@interface ViewController ()<UITableViewDelegate, UITableViewDataSource, FissionSdkLinkDelegate>

@property (nonatomic, strong) NSArray *sortedArray;


@property (nonatomic, strong) NSString *recordId;
@property (nonatomic, strong) NSString *missionId;

@property (nonatomic, strong) NSString *inviteMissionId;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[FissionSdkLink shareInstance] start:self];
    
    _sortedArray = @[@{
                         @"用户": @[@"游客注册",@"绑定社交账号",@"微信用户注册"]},
                     @{@"提现":@[@"获取提现信息",@"提现请求",@"提现微信"]},
                     @{@"分享":@[@"打开分享页面"]},
                     @{@"宗门":@[@"获取弟子信息",@"招募奖励",@"产生贡献", @"现金兑换"]},
                       @{@"激励任务":@[@"获取激励任务",@"激励任务记录查询",@"今日金币获取信息查询"]}
    ];
    
    UILabel *titleLab =  [[UILabel alloc] initWithFrame:CGRectMake(0, kTopBarSafeHeight, ScreenWidth, 20)];
    titleLab.text = @"Test";
    [titleLab setTextAlignment:NSTextAlignmentCenter];
    [self.view addSubview:titleLab];
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, kTopBarSafeHeight+20, ScreenWidth, 1)];
    line.backgroundColor = [UIColor grayColor];
    [self.view addSubview:line];
    
    UITableView *apisTab = [[UITableView alloc] initWithFrame:CGRectMake(0, kTopBarSafeHeight+21, ScreenWidth, ScreenHeight-kTopBarSafeHeight-21)];
    apisTab.delegate = self;
    apisTab.dataSource = self;
    apisTab.userInteractionEnabled = YES;
       
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor clearColor];
    [apisTab setTableFooterView:view];
    
       
    [self.view addSubview:apisTab];
}

#pragma  mark FissionSdkLinkDelegate
- (void)getInstallParams: (NSDictionary *)params {
    NSString *inviterId = params[@"inviter_id"];
    NSString *msg  = [NSString stringWithFormat:@"get install params: %@", inviterId];
    dispatch_async(dispatch_get_main_queue(), ^{
    [self.view makeToast:msg duration:5.0 position:CSToastPositionCenter];
    });
    
    NSString *userId = [[NSUserDefaults  standardUserDefaults] stringForKey:@"fissionsdkdemo_test_userid"];
    if (userId == nil || [userId isEqualToString:@""]) {
        [FissionAccountUser registerUserId:inviterId initInfo:nil success:^(FissionAccountUserData * _Nonnull userData) {
            NSLog(@"*******registerUserId测试成功: userData: %@", [userData description]);
            NSString *userId = userData.id;
            [[NSUserDefaults  standardUserDefaults] setObject:userId forKey:@"fissionsdkdemo_test_userid"];
        } failure:^(NSError * _Nonnull error) {
            NSLog(@"*******registerUserId测试失败: errorCode: %zd, message:%@", error.code, error.localizedDescription);
        }];
    } else {
        [FissionAccountUser bindInviter:inviterId success:^{
            NSLog(@"*******bindInviter测试成功");
        } failure:^(NSError * _Nonnull error) {
            NSLog(@"*******bindInviter测试失败: errorCode: %zd, message:%@", error.code, error.localizedDescription);
        }];
    }
}

#pragma mark <UITableViewDelegate>
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _sortedArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *testItem = [_sortedArray[section] allValues][0];
    return testItem.count;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 20;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 20)];
    view.backgroundColor = [UIColor grayColor];
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 2, tableView.frame.size.width-20, 16)];
    
    headerLabel.font = [UIFont boldSystemFontOfSize:15.0];
    headerLabel.textColor = [UIColor whiteColor];
    headerLabel.text = [_sortedArray[section] allKeys][0];
    
    [view addSubview:headerLabel];
   
    return view;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"apiCellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] init];
    }
    
    NSArray *name = [_sortedArray[indexPath.section] allValues][0];
    
    cell.textLabel.text = name[indexPath.row];
    [cell.textLabel setTextColor:[UIColor colorWithRed:28.0/255.0 green:147.0/255.0 blue:243.0/255.0 alpha:1.0]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *names = [_sortedArray[indexPath.section] allValues][0];
    
    NSString *testItem = names[indexPath.row];
    switch (indexPath.section) {
        case 0://用户测试
            [self userAPITest:testItem];
            break;
            
        case 1://提现测试
            [self withdrawAPITest:testItem];
            break;
        
        case 2://分享测试
            [self shareTest:testItem];
            break;
            
        case 3://宗门测试
            [self sectTest:testItem];
            break;
            
        case 4://任务测试
            [self missionAPITest:testItem];
            break;
            
        default:
            break;
    }
}

- (void)toastFailureInfo:(NSString *)functionName error:(NSError *)error {
    NSLog(@"******* %@ ******* 测试失败: errorCode: %zd, message:%@",functionName, error.code, error.localizedDescription);
    NSString *message = [NSString stringWithFormat:@"%@ 测试失败，errorCode: %zd, message: %@", functionName, error.code, error.localizedDescription];
    dispatch_async(dispatch_get_main_queue(), ^{
    [self.view makeToast:message duration:5.0 position:CSToastPositionCenter];
    });
}


- (void)userAPITest:(NSString *)testItem {
    if ([testItem isEqualToString:@"游客注册"]) {
        [FissionAccountUser registerUserId:nil initInfo: nil success:^(FissionAccountUserData *userData) {
            NSLog(@"*******registerUserId测试成功: userData: %@", [userData description]);
            NSString *userId = userData.id;
            [[NSUserDefaults  standardUserDefaults] setObject:userId forKey:@"fissionsdkdemo_test_userid"];
            [[NSUserDefaults  standardUserDefaults]  synchronize];
        } failure:^(NSError *error) {
            [self toastFailureInfo:@"registerUserId" error:error];
        }];
    } else if ([testItem isEqualToString:@"绑定社交账号"] || [testItem isEqualToString:@"社交账号绑定检查"]) {
        //todo 进入下一个页面登录社交账号
        WXLoginViewController *loginVC = [[WXLoginViewController alloc] init];
        loginVC.modalPresentationStyle = UIModalPresentationFullScreen;
        [self presentViewController:loginVC animated:YES completion:nil];
    } else if ([testItem isEqualToString:@"获取用户信息"]) {
        [FissionAccountUser getUserInfo:^(FissionAccountUserData *userData) {
            NSLog(@"*******getUserInfo测试成功: userData: %@", [userData description]);
        } failure:^(NSError *error) {
            [self toastFailureInfo:@"getUserInfo" error:error];
        }];
    }
}

- (void)withdrawAPITest:(NSString *)testItem {
    if ([testItem isEqualToString:@"获取提现信息"]) {
        [FissionSdkWithdraw getWithdrawInfo:^(FissionSdkWithdrawData *data) {
            NSLog(@"*******getWithdrawInfo测试成功: data: %@", [data description]);
        } failure:^(NSError *error){
             [self toastFailureInfo:@"getWithdrawInfo" error:error];
        }];
    } else if ([testItem isEqualToString:@"提现请求"]) {
        [FissionSdkWithdraw requestWithdraw:WITHDRAW_TEST_MISSION_ID realName:@"张三" idCard:@"320XXXXX" payRemark:@"红包" phoneNo:@"1334566777" channel:nil success:^(FissionSdkWithdrawResult *result) {
             //返回签到记录
            NSLog(@"*******requestWithdraw测试成功: result: %@", [result description]);
        }  failure:^(NSError *error){
             [self toastFailureInfo:@"requestWithdraw" error:error];
        }];
    } else if ([testItem isEqualToString:@"微信提现"]) {
        [FissionSdkWithdraw requestWXPay:WXWITHDRAW_TEST_MISSION_ID payRemark:@"每日任务" comment:nil  success:^(FissionSdkWithdrawResult *result) {
             NSLog(@"*******requestWXPay测试成功: result: %@", [result description]);
        } failure:^(NSError *error){
             [self toastFailureInfo:@"requestWXPay" error:error];
        }];
    }
}

- (void)shareTest:(NSString *)testItem {
    ShareViewController *vc = [[ShareViewController alloc] init];
    
    [self presentViewController:vc animated:YES completion:nil];
}

- (void)sectTest:(NSString *)testItem {
    if ([testItem isEqualToString:@"获取弟子信息"]) {
        ApprenticeViewController *vc = [[ApprenticeViewController alloc] init];
        
        [self presentViewController:vc animated:YES completion:nil];
    } else if ([testItem isEqualToString:@"招募奖励"]) {
       InviteAwardViewController *vc = [[InviteAwardViewController alloc] init];
        
        [self presentViewController:vc animated:YES completion:nil];
    } else if ([testItem isEqualToString:@"产生贡献"]) {
        [FissionSdkSect genContribution:0 success:^(int star) {
            NSLog(@"*******genContribution测试成功: star :%d", star);
        } failure:^(NSError * _Nonnull error) {
            [self toastFailureInfo:@"genContribution" error:error];
        }];
    } else if ([testItem isEqualToString:@"现金兑换"]) {
        [FissionSdkSect transform:^(FissionSdkSectTransformResult * _Nonnull result) {
            NSLog(@"******* transform ******* 测试成功: %@", [result description]);
        } failure:^(NSError * _Nonnull error) {
            [self toastFailureInfo:@"transform" error:error];
        }];
    }
}

- (void)missionAPITest:(NSString *)testItem {
    if ([testItem isEqualToString:@"获取激励任务"]) {
        MissionViewController *vc = [[MissionViewController alloc] init];
        [self presentViewController:vc animated:YES completion:nil];
    } else if ([testItem isEqualToString:@"激励任务记录查询"]) {
        [FissionSdkMission missionQuary:nil days:1 pageSize:0 pageIndex:0 success:^(FissionSdkMissionQueryResult *result) {
             NSLog(@"*******missionQuary测试成功:  result:%@", [result description]);
        } failure:^(NSError *error){
             [self toastFailureInfo:@"missionQuary" error:error];
         }];
    } else if ([testItem isEqualToString:@"今日金币获取信息查询"]) {
        [FissionSdkMission getTodayCoins:^(FissionSdkMissionTodayCoinsResult *result) {
            //今日金币获取信息
            NSLog(@"*******getTodayCoins测试成功:  result:%@", [result description]);
        } failure:^(NSError *error){
            [self toastFailureInfo:@"getTodayCoins" error:error];
        }];
    }
}


@end

